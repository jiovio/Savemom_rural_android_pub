package com.jiovio.olivewear.rural.savemom.adapter

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.api.model.Checkup
import com.jiovio.olivewear.rural.savemom.api.model.Patient
import com.jiovio.olivewear.rural.savemom.view.QuickSandTextView

/**
 * Created by abara on 23/08/17.
 */
class FeedbackItemAdapter(private val patients: List<Patient>, private val clickAction: (title: String, position: Int) -> Unit) : RecyclerView.Adapter<FeedbackItemAdapter.FeedbackItemHolder>() {

    private lateinit var context: Context

    override fun onBindViewHolder(holder: FeedbackItemHolder, position: Int) {

        val patient = patients[holder.adapterPosition]
        val noOfCheckups = patient.checkup?.size ?: 0
        val checkup: Checkup? = if (noOfCheckups != 0) patient.checkup?.get(noOfCheckups.minus(1)) else null

        holder.title.text = patient.name
        holder.desc.text = context.resources.getString(R.string.feedback_last_update, checkup?.date ?: "Never")
        holder.layout.setOnClickListener {
            clickAction(patient.name!!, holder.adapterPosition)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedbackItemHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.single_item_feedback, parent, false)
        return FeedbackItemHolder(view)
    }

    override fun getItemCount(): Int = patients.size

    class FeedbackItemHolder(view: View) : RecyclerView.ViewHolder(view) {
        val layout = view.findViewById<ConstraintLayout>(R.id.item_feedback_layout)
        val title = view.findViewById<QuickSandTextView>(R.id.item_feedback_title)
        val desc = view.findViewById<QuickSandTextView>(R.id.item_feedback_desc)
        val color = view.findViewById<AppCompatImageView>(R.id.item_feedback_color)
    }
}