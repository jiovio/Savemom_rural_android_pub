package com.jiovio.olivewear.rural.savemom.view

import android.content.Context
import android.graphics.Typeface
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import com.jiovio.olivehealth.rural.savemom.R

/**
 * Created by abara on 10/07/17.
 */

class QuickSandTextView : AppCompatTextView {
    constructor(context: Context) : super(context) {
        initFont(context, null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initFont(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initFont(context, attrs)
    }

    private fun initFont(context: Context, attrs: AttributeSet?) {

        val attrsArray = context.obtainStyledAttributes(attrs, R.styleable.QuickSandTextView)

        val textType = attrsArray.getInt(R.styleable.QuickSandTextView_textType, 1)

        var typeface = Typeface.createFromAsset(getContext().assets, "fonts/Quicksand-Regular.ttf")
        when (textType) {
            1 // normal
            -> typeface = Typeface.createFromAsset(getContext().assets, "fonts/Quicksand-Regular.ttf")
            2 // medium
            -> typeface = Typeface.createFromAsset(getContext().assets, "fonts/Quicksand-Medium.ttf")
            3 // bold
            -> typeface = Typeface.createFromAsset(getContext().assets, "fonts/Quicksand-Bold.ttf")
        }
        setTypeface(typeface)

        attrsArray.recycle()
    }
}
