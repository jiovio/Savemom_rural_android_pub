package com.jiovio.olivewear.rural.savemom.api.model

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by abara on 11/09/17.
 */
class Patient() : Parcelable {

    var id: String? = null
    var name: String? = null
    var aadharno: String? = null
    var fathername: String? = null
    var husbandname: String? = null
    var villagename: String? = null
    var district: String? = null
    var state: String? = null
    var pincode: String? = null
    var age: String? = null
    var duedate: String? = null
    var phoneno: String? = null
    var childdelivered: String? = null
    var complications: String? = null
    var nooflastcarevisit: String? = null
    var childborn: String? = null
    var ageofchild: String? = null
    var firstchildbornage: String? = null
    var diabetics: String? = null
    var bp: String? = null
    var cancer: String? = null
    var othermedicalissue: String? = null
    var diabeticsparent: String? = null
    var bpparent: String? = null
    var cancerparent: String? = null
    var othermedicalissueparent: String? = null
    var coffee: String? = null
    var tea: String? = null
    var tobaccoo: String? = null
    var alhocol: String? = null
    var tobaccoohusband: String? = null
    var alhocolhusband: String? = null
    var hiv: String? = null
    var sexualabuse: String? = null
    internal var checkup: List<Checkup>? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        name = parcel.readString()
        aadharno = parcel.readString()
        fathername = parcel.readString()
        husbandname = parcel.readString()
        villagename = parcel.readString()
        district = parcel.readString()
        state = parcel.readString()
        pincode = parcel.readString()
        age = parcel.readString()
        duedate = parcel.readString()
        phoneno = parcel.readString()
        childdelivered = parcel.readString()
        complications = parcel.readString()
        nooflastcarevisit = parcel.readString()
        childborn = parcel.readString()
        ageofchild = parcel.readString()
        firstchildbornage = parcel.readString()
        diabetics = parcel.readString()
        bp = parcel.readString()
        cancer = parcel.readString()
        othermedicalissue = parcel.readString()
        diabeticsparent = parcel.readString()
        bpparent = parcel.readString()
        cancerparent = parcel.readString()
        othermedicalissueparent = parcel.readString()
        coffee = parcel.readString()
        tea = parcel.readString()
        tobaccoo = parcel.readString()
        alhocol = parcel.readString()
        tobaccoohusband = parcel.readString()
        alhocolhusband = parcel.readString()
        hiv = parcel.readString()
        sexualabuse = parcel.readString()
        checkup = parcel.createTypedArrayList(Checkup)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(aadharno)
        parcel.writeString(fathername)
        parcel.writeString(husbandname)
        parcel.writeString(villagename)
        parcel.writeString(district)
        parcel.writeString(state)
        parcel.writeString(pincode)
        parcel.writeString(age)
        parcel.writeString(duedate)
        parcel.writeString(phoneno)
        parcel.writeString(childdelivered)
        parcel.writeString(complications)
        parcel.writeString(nooflastcarevisit)
        parcel.writeString(childborn)
        parcel.writeString(ageofchild)
        parcel.writeString(firstchildbornage)
        parcel.writeString(diabetics)
        parcel.writeString(bp)
        parcel.writeString(cancer)
        parcel.writeString(othermedicalissue)
        parcel.writeString(diabeticsparent)
        parcel.writeString(bpparent)
        parcel.writeString(cancerparent)
        parcel.writeString(othermedicalissueparent)
        parcel.writeString(coffee)
        parcel.writeString(tea)
        parcel.writeString(tobaccoo)
        parcel.writeString(alhocol)
        parcel.writeString(tobaccoohusband)
        parcel.writeString(alhocolhusband)
        parcel.writeString(hiv)
        parcel.writeString(sexualabuse)
        parcel.writeTypedList(checkup)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Patient> {
        override fun createFromParcel(parcel: Parcel): Patient {
            return Patient(parcel)
        }

        override fun newArray(size: Int): Array<Patient?> {
            return arrayOfNulls(size)
        }
    }

}

class Bp() : Parcelable {

    var systolic: String? = null
    var diastolic: String? = null
    var pp: String? = null
    var pulse: String? = null

    constructor(parcel: Parcel) : this() {
        systolic = parcel.readString()
        diastolic = parcel.readString()
        pp = parcel.readString()
        pulse = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(systolic)
        parcel.writeString(diastolic)
        parcel.writeString(pp)
        parcel.writeString(pulse)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Bp> {
        override fun createFromParcel(parcel: Parcel): Bp {
            return Bp(parcel)
        }

        override fun newArray(size: Int): Array<Bp?> {
            return arrayOfNulls(size)
        }
    }

}

class Checkup() : Parcelable {

    var date: String? = null
    var vitals: Vitals? = null
    var symptoms: Symptoms? = null
    var reports: List<Report>? = null
    var isDone: Boolean = false
    var color: String? = null
    var userid: String? = null
    var comments: List<Comment>? = null
    var prescription: List<Prescription>? = null

    constructor(parcel: Parcel) : this() {
        date = parcel.readString()
        vitals = parcel.readParcelable(Vitals::class.java.classLoader)
        symptoms = parcel.readParcelable(Symptoms::class.java.classLoader)
        reports = parcel.createTypedArrayList(Report)
        isDone = parcel.readByte() != 0.toByte()
        color = parcel.readString()
        userid = parcel.readString()
        comments = parcel.createTypedArrayList(Comment)
        prescription = parcel.createTypedArrayList(Prescription)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(date)
        parcel.writeParcelable(vitals, flags)
        parcel.writeParcelable(symptoms, flags)
        parcel.writeTypedList(reports)
        parcel.writeByte(if (isDone) 1 else 0)
        parcel.writeString(color)
        parcel.writeString(userid)
        parcel.writeTypedList(comments)
        parcel.writeTypedList(prescription)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Checkup> {
        override fun createFromParcel(parcel: Parcel): Checkup {
            return Checkup(parcel)
        }

        override fun newArray(size: Int): Array<Checkup?> {
            return arrayOfNulls(size)
        }
    }

}

class Comment() : Parcelable {

    var username: String? = null
    var roll: String? = null
    var comment: String? = null
    var date: String? = null
    var commentdate: String? = null
    var commenttime: String? = null
    var myself: String? = null
    var isEmergency: Boolean = false

    constructor(parcel: Parcel) : this() {
        username = parcel.readString()
        roll = parcel.readString()
        comment = parcel.readString()
        date = parcel.readString()
        commentdate = parcel.readString()
        commenttime = parcel.readString()
        myself = parcel.readString()
        isEmergency = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(username)
        parcel.writeString(roll)
        parcel.writeString(comment)
        parcel.writeString(date)
        parcel.writeString(commentdate)
        parcel.writeString(commenttime)
        parcel.writeString(myself)
        parcel.writeByte(if (isEmergency) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Comment> {
        override fun createFromParcel(parcel: Parcel): Comment {
            return Comment(parcel)
        }

        override fun newArray(size: Int): Array<Comment?> {
            return arrayOfNulls(size)
        }
    }

}

class Datum() : Parcelable {

    var tabletname: String? = null
    var dosage: String? = null
    var morning: String? = null
    var morningfood: String? = null
    var night: String? = null
    var nightfood: String? = null

    constructor(parcel: Parcel) : this() {
        tabletname = parcel.readString()
        dosage = parcel.readString()
        morning = parcel.readString()
        morningfood = parcel.readString()
        night = parcel.readString()
        nightfood = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(tabletname)
        parcel.writeString(dosage)
        parcel.writeString(morning)
        parcel.writeString(morningfood)
        parcel.writeString(night)
        parcel.writeString(nightfood)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Datum> {
        override fun createFromParcel(parcel: Parcel): Datum {
            return Datum(parcel)
        }

        override fun newArray(size: Int): Array<Datum?> {
            return arrayOfNulls(size)
        }
    }

}

class Prescription() : Parcelable {

    var date: String? = null
    var prescriptiondate: String? = null
    var prescriptiontime: String? = null
    var data: List<Datum>? = null

    constructor(parcel: Parcel) : this() {
        date = parcel.readString()
        prescriptiondate = parcel.readString()
        prescriptiontime = parcel.readString()
        data = parcel.createTypedArrayList(Datum.CREATOR)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(date)
        parcel.writeString(prescriptiondate)
        parcel.writeString(prescriptiontime)
        parcel.writeTypedList(data)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Prescription> {
        override fun createFromParcel(parcel: Parcel): Prescription {
            return Prescription(parcel)
        }

        override fun newArray(size: Int): Array<Prescription?> {
            return arrayOfNulls(size)
        }
    }

}

class Report() : Parcelable {

    var reportname: String? = null
    var reportimg: String? = null

    constructor(parcel: Parcel) : this() {
        reportname = parcel.readString()
        reportimg = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(reportname)
        parcel.writeString(reportimg)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Report> {
        override fun createFromParcel(parcel: Parcel): Report {
            return Report(parcel)
        }

        override fun newArray(size: Int): Array<Report?> {
            return arrayOfNulls(size)
        }
    }

}

class Spo2() : Parcelable {

    var pulse: String? = null
    var oxygen: String? = null

    constructor(parcel: Parcel) : this() {
        pulse = parcel.readString()
        oxygen = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(pulse)
        parcel.writeString(oxygen)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Spo2> {
        override fun createFromParcel(parcel: Parcel): Spo2 {
            return Spo2(parcel)
        }

        override fun newArray(size: Int): Array<Spo2?> {
            return arrayOfNulls(size)
        }
    }

}

class Symptoms() : Parcelable {

    var headache: String? = null
    var blurredvision: String? = null
    var dizziness: String? = null
    var shortnessofbreath: String? = null
    var legedema: String? = null
    var chestpain: String? = null
    var abdominalpain: String? = null
    var vomiting: String? = null
    var sourtaste: String? = null
    var stomachburning: String? = null
    var spotting: String? = null
    var bleeding: String? = null
    var noofpads: String? = null
    var amnioticfluid: String? = null
    var urinaryburning: String? = null
    var vaginalitching: String? = null
    var vaginaldischarge: String? = null
    var colorofdischarge: String? = null
    var urinaryfrequency: String? = null
    var feverandchills: String? = null
    var rashesonskin: String? = null
    var coughandphlegm: String? = null
    var colorofphlegm: String? = null
    var asthma: String? = null
    var bodyaches: String? = null
    var anydiscomforts: String? = null

    constructor(parcel: Parcel) : this() {
        headache = parcel.readString()
        blurredvision = parcel.readString()
        dizziness = parcel.readString()
        shortnessofbreath = parcel.readString()
        legedema = parcel.readString()
        chestpain = parcel.readString()
        abdominalpain = parcel.readString()
        vomiting = parcel.readString()
        sourtaste = parcel.readString()
        stomachburning = parcel.readString()
        spotting = parcel.readString()
        bleeding = parcel.readString()
        noofpads = parcel.readString()
        amnioticfluid = parcel.readString()
        urinaryburning = parcel.readString()
        vaginalitching = parcel.readString()
        vaginaldischarge = parcel.readString()
        colorofdischarge = parcel.readString()
        urinaryfrequency = parcel.readString()
        feverandchills = parcel.readString()
        rashesonskin = parcel.readString()
        coughandphlegm = parcel.readString()
        colorofphlegm = parcel.readString()
        asthma = parcel.readString()
        bodyaches = parcel.readString()
        anydiscomforts = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(headache)
        parcel.writeString(blurredvision)
        parcel.writeString(dizziness)
        parcel.writeString(shortnessofbreath)
        parcel.writeString(legedema)
        parcel.writeString(chestpain)
        parcel.writeString(abdominalpain)
        parcel.writeString(vomiting)
        parcel.writeString(sourtaste)
        parcel.writeString(stomachburning)
        parcel.writeString(spotting)
        parcel.writeString(bleeding)
        parcel.writeString(noofpads)
        parcel.writeString(amnioticfluid)
        parcel.writeString(urinaryburning)
        parcel.writeString(vaginalitching)
        parcel.writeString(vaginaldischarge)
        parcel.writeString(colorofdischarge)
        parcel.writeString(urinaryfrequency)
        parcel.writeString(feverandchills)
        parcel.writeString(rashesonskin)
        parcel.writeString(coughandphlegm)
        parcel.writeString(colorofphlegm)
        parcel.writeString(asthma)
        parcel.writeString(bodyaches)
        parcel.writeString(anydiscomforts)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Symptoms> {
        override fun createFromParcel(parcel: Parcel): Symptoms {
            return Symptoms(parcel)
        }

        override fun newArray(size: Int): Array<Symptoms?> {
            return arrayOfNulls(size)
        }
    }

}

class Vitals() : Parcelable {

    var bp: Bp? = null
    var spo2: Spo2? = null
    var fhr: String? = null
    var glucose: String? = null
    var weight: String? = null

    constructor(parcel: Parcel) : this() {
        bp = parcel.readParcelable(Bp::class.java.classLoader)
        spo2 = parcel.readParcelable(Spo2::class.java.classLoader)
        fhr = parcel.readString()
        glucose = parcel.readString()
        weight = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(bp, flags)
        parcel.writeParcelable(spo2, flags)
        parcel.writeString(fhr)
        parcel.writeString(glucose)
        parcel.writeString(weight)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Vitals> {
        override fun createFromParcel(parcel: Parcel): Vitals {
            return Vitals(parcel)
        }

        override fun newArray(size: Int): Array<Vitals?> {
            return arrayOfNulls(size)
        }
    }

}

class NewPatient() : Parcelable {
    override fun writeToParcel(p0: Parcel, p1: Int) {
        p0.writeString(_id)
        p0.writeValue(success)
        p0.writeString(message)
    }

    var _id: String? = null
    var success: Boolean? = null
    var message: String? = null

    constructor(parcel: Parcel) : this() {
        _id = parcel.readString()
        success = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        message = parcel.readString()
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<NewPatient> {
        override fun createFromParcel(parcel: Parcel): NewPatient {
            return NewPatient(parcel)
        }

        override fun newArray(size: Int): Array<NewPatient?> {
            return arrayOfNulls(size)
        }
    }

}