package com.jiovio.olivewear.rural.savemom.fragment

import android.app.DatePickerDialog
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jiovio.olivehealth.rural.savemom.R
import org.jetbrains.anko.find
import java.util.*

/**
 * Created by abara on 14/09/17.
 */

class NewUserFragments : BaseFragment() {

    companion object {
        val PERSONAL_INFO = 1;
        val MEDICAL_INFO = 2;
        val SOCIAL_INFO = 3;
        fun get(fragmentType: Int): NewUserFragments {
            val fragment = NewUserFragments()
            val args = Bundle()
            args.putInt("type", fragmentType)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val type = arguments.getInt("type")
        return when (type) {
            PERSONAL_INFO -> {
                val view = inflater.inflate(R.layout.fragment_newuser_personalinfo, container, false)
                val cal = Calendar.getInstance()
                val dueDateBox = view.find<TextInputEditText>(R.id.newuser_personalinfo_duedate_name)

                val dateSetListener = DatePickerDialog.OnDateSetListener { _, year, month, day ->
                    cal.set(year, month, day)
                    val date = java.text.SimpleDateFormat("dd/MM/yyyy", Locale.US).format(cal.time)
                    dueDateBox.setText(date)
                }

                dueDateBox.setOnClickListener {
                    DatePickerDialog(activity, dateSetListener, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH))
                            .show()
                }
                view
            }
            MEDICAL_INFO -> inflater.inflate(R.layout.fragment_newuser_medicalhx, container, false)
            SOCIAL_INFO -> inflater.inflate(R.layout.fragment_newuser_socialhx, container, false)
            else -> null
        }

    }

}