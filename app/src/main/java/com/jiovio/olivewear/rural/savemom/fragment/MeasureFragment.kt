package com.jiovio.olivewear.rural.savemom.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.mcloud.DeviceConnectionDelegate
import com.jiovio.olivewear.rural.savemom.mcloud.MeasureHelper
import com.jiovio.olivewear.rural.savemom.mcloud.OliveMeasureHelper
import com.jiovio.olivewear.rural.savemom.ui.MeasureActivity
import com.jiovio.olivewear.rural.savemom.util.BluetoothUtils
import kotlinx.android.synthetic.main.fragment_bp_measure.*
import org.jetbrains.anko.support.v4.toast

/**
 * Created by abara on 28/08/17.
 */
class MeasureFragment : BaseFragment(), OliveMeasureHelper.OliveWearListener {

    interface OnMeasureSuccessListener {
        fun onMeasureSuccess(fragmentType: Int, value: String)
    }

    private var measureCallback: OnMeasureSuccessListener? = null

    override fun onDeviceConnecting() {
        toast("Connecting...")
    }

    override fun onDeviceSearch() {
        toast("Searching...")
    }

    override fun onDeviceNotFound() {
        toast("Device not found!")
    }

    override fun onDeviceConnectionFailure() {
        toast("Device connection failed!")
    }

    override fun onDeviceConnected() {
        toast("Device connected!")
        measureHelper.measure()
    }

    override fun onDeviceDisconnected() {
        toast("Device disconnected!")
    }

    override fun onDeviceException() {
        toast("Device exception!")
    }

    override fun onDevicePowerLow() {
        toast("Device Battery low!")
    }

    override fun onProgress() {
        toast("Measuring...!")
    }

    override fun onResult(result: String) {
        toast("Result:" + result)
        fragment_bp_measure_status.text = result
    }

    private var currentFragmentType = MeasureActivity.FragmentType.BP
    private var currentMeasureType = MeasureActivity.FragmentType.BP
    private val measureHelper: OliveMeasureHelper = OliveMeasureHelper()
    private val deviceConnectionDelegate = DeviceConnectionDelegate.newInstance(getDeviceName(), {
        measureHelper.init(this@MeasureFragment, getMeasureType(), it)
    })

    companion object {
        private val EXTRA_TYPE = "extra_type"
        fun getInstance(fragmentType: String): MeasureFragment {
            val fragment = MeasureFragment()
            val args = Bundle()
            args.putString(EXTRA_TYPE, fragmentType)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        currentMeasureType = MeasureActivity.FragmentType.valueOf(arguments.getString(EXTRA_TYPE, MeasureActivity.FragmentType.BP.name))
        return inflater.inflate(R.layout.fragment_bp_measure, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toolbarColor(getBgColor(), R.color.colorWhite)

        if (currentFragmentType != MeasureActivity.FragmentType.BG) {
            BluetoothUtils.startDiscovery()
        }

        fragment_bp_measure_done.setOnClickListener {
            nextFragment(MeasureFragment.getInstance(currentFragmentType.name), false)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            measureCallback = context as OnMeasureSuccessListener
        } catch (e: ClassCastException) {
            throw ClassCastException("Class must implement OnMeasureSuccessListener")
        }
        when (currentFragmentType) {
            MeasureActivity.FragmentType.SPO -> measureWithBluetoothDevice()
            MeasureActivity.FragmentType.BG -> measureWithAudioDevice()
            MeasureActivity.FragmentType.TEMPERATURE -> measureWithBluetoothDevice()
            else -> measureWithBluetoothDevice()
        }
    }

    private fun getBgColor(): Int = when (currentFragmentType) {
        MeasureActivity.FragmentType.BP -> R.color.colorTeal
        else -> R.color.colorTeal
    }

    private fun getMeasureType(): Int = when (currentFragmentType) {
        MeasureActivity.FragmentType.SPO -> MeasureHelper.BLOOD_OXYGEN_ALL
        MeasureActivity.FragmentType.BG -> MeasureHelper.BLOOD_SUGAR
        MeasureActivity.FragmentType.TEMPERATURE -> MeasureHelper.BASE_TEMPERATURE
        else -> MeasureHelper.BLOOD_PRESSURE_ALL
    }

    private fun getDeviceName(): String = when (currentFragmentType) {
        MeasureActivity.FragmentType.SPO -> "mCloud-O"
        MeasureActivity.FragmentType.BG -> "mCloud-BG"
        MeasureActivity.FragmentType.TEMPERATURE -> "mCloud-T"
        MeasureActivity.FragmentType.WEIGHT -> "mCloud-W"
        else -> "mCloud-P"
    }


    fun getTitle(): Int = when (currentFragmentType) {
        MeasureActivity.FragmentType.BG -> R.string.blood_glucose_title
        else -> R.string.blood_pressure_title
    }

    private fun measureWithAudioDevice() {
        measureHelper.init(this@MeasureFragment, getMeasureType(), "mCloud-BG")
    }

    private fun measureWithBluetoothDevice() {
        val pairedOliveWear = BluetoothUtils.getPairedDevices().find { it.bluetoothDevice.name == getDeviceName() }
        if (pairedOliveWear != null) {
            measureHelper.init(this@MeasureFragment, getMeasureType(),
                    pairedOliveWear.bluetoothDevice.address)
        } else {
            deviceConnectionDelegate.attachReceiver(activity)
        }
    }

    override fun onDetach() {
        super.onDetach()
        deviceConnectionDelegate.detachReceiver(activity)
        measureHelper.release()
    }

}