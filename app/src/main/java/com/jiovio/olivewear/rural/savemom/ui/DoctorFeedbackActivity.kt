package com.jiovio.olivewear.rural.savemom.ui

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.MenuItem
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.adapter.TabsAdapter
import com.jiovio.olivewear.rural.savemom.fragment.BaseFragment
import com.jiovio.olivewear.rural.savemom.fragment.ListFragment
import com.jiovio.olivewear.rural.savemom.fragment.OnlineDocDetailsFragment
import com.jiovio.olivewear.rural.savemom.fragment.OnlineDocFragment
import com.jiovio.olivewear.rural.savemom.util.commitTransaction
import com.jiovio.olivewear.rural.savemom.util.parseColor
import kotlinx.android.synthetic.main.activity_doctor_feedback.*
import org.jetbrains.anko.textColor

/**
 * Created by abara on 23/08/17.
 */
class DoctorFeedbackActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_doctor_feedback)
        doctor_appbar.title = ""
        setSupportActionBar(doctor_appbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val prescription = intent?.extras?.getBoolean("prescription") ?: false
        var fragment: ListFragment = OnlineDocFragment()
        if (prescription) {
            fragment = OnlineDocDetailsFragment()
        }

        val title = resources.getStringArray(R.array.doctor_feedback_tabs)

        doctor_viewpager.adapter = TabsAdapter(title,
                arrayOf(
                        fragment,
                        ListFragment()),
                supportFragmentManager)
        doctor_tablayout.setupWithViewPager(doctor_viewpager)

    }

    override fun changeToolbarColor(color: Int, titleColor: Int) {
        supportActionBar?.setBackgroundDrawable(ColorDrawable(parseColor(color)))
        doctor_appbar_title.textColor = parseColor(titleColor)
    }

    override fun loadFragment(fragment: BaseFragment, addToBackStack: Boolean) {
        supportFragmentManager.commitTransaction {
            if (addToBackStack) addToBackStack(null)
            replace(R.id.doctor_viewpager, fragment)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }


}