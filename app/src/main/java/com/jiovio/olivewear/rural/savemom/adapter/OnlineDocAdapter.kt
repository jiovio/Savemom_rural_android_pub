package com.jiovio.olivewear.rural.savemom.adapter

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.view.QuickSandTextView

/**
 * Created by abara on 26/08/17.
 */
class OnlineDocAdapter(private val addTaskAction: () -> Unit, private val clickAction: () -> Unit) : RecyclerView.Adapter<OnlineDocAdapter.OnlineDocHolder>() {

    override fun onBindViewHolder(holder: OnlineDocHolder, position: Int) {
        holder.layout.setOnClickListener {
            clickAction()
        }
        holder.addToTask.setOnClickListener {
            addTaskAction()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OnlineDocHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.single_item_online_doc, parent, false)
        return OnlineDocHolder(view)
    }

    override fun getItemCount(): Int = 3


    class OnlineDocHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title = view.findViewById<QuickSandTextView>(R.id.item_online_doc_title)
        val desc = view.findViewById<QuickSandTextView>(R.id.item_online_doc_desc)
        val addToTask = view.findViewById<AppCompatButton>(R.id.item_online_doc_addtask)
        val details = view.findViewById<AppCompatButton>(R.id.item_online_doc_details)
        val layout = view.findViewById<ConstraintLayout>(R.id.item_online_doc_layout)
    }

}