package com.jiovio.olivewear.rural.savemom.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.api.model.Checkup

/**
 * Created by abara on 26/08/17.
 */
class FeedbackUserAdapter(private val checkups: ArrayList<Checkup>, private val clickAction: (title: String, position: Int) -> Unit) : RecyclerView.Adapter<FeedbackItemAdapter.FeedbackItemHolder>() {

    private lateinit var context: Context
    private var lastCheckup: String? = null

    init {
        val size = checkups.size
        lastCheckup = checkups[size - 1].date
    }

    override fun onBindViewHolder(holder: FeedbackItemAdapter.FeedbackItemHolder, position: Int) {
        val checkup = checkups[position]
        holder.title.text = context.resources.getString(R.string.feedback_test_title, checkup.date)
        holder.desc.text = context.resources.getString(R.string.feedback_last_update, checkup.date)
        holder.layout.setOnClickListener {
            clickAction(holder.title.text.toString(), holder.adapterPosition)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedbackItemAdapter.FeedbackItemHolder {
        this.context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.single_item_feedback, parent, false)
        return FeedbackItemAdapter.FeedbackItemHolder(view)
    }

    override fun getItemCount(): Int = checkups.size

}