package com.jiovio.olivewear.rural.savemom.fragment

import android.os.Bundle
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.adapter.FeedbackTestAdapter
import com.jiovio.olivewear.rural.savemom.api.model.Checkup
import com.jiovio.olivewear.rural.savemom.ui.DoctorFeedbackActivity
import com.jiovio.olivewear.rural.savemom.ui.SymptomsActivity
import kotlinx.android.synthetic.main.fragment_list.*
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.toast

/**
 * Created by abara on 17/09/17.
 */
class FeedbackTestsFragment : ListFragment() {

    companion object {
        fun getInstance(title: String, checkups: ArrayList<Checkup>?): FeedbackTestsFragment {
            val fragment = FeedbackTestsFragment()
            val args = Bundle()
            args.putParcelableArrayList("checkups", checkups)
            args.putString("title", title)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val toolbarTitle = arguments.getString("title")
        setTitle(toolbarTitle)

        val checkups = arguments.getParcelableArrayList<Checkup>("checkups")

        if (checkups != null) {

            val testPairs = generatePairList(checkups)
            fragment_list.adapter = FeedbackTestAdapter(testPairs, { pos ->
                // TODO: Display the "FeedbackTestAdapter"
                // TODO: Add actions to first 3 items
                when (pos) {
                    0 -> startActivity<DoctorFeedbackActivity>()
                    1 -> startActivity<DoctorFeedbackActivity>("prescription" to true)
                    2 -> startActivity<SymptomsActivity>() // Reports activity
                    3 -> {
                        val symptoms = checkups[0].symptoms
                        if (symptoms != null)
                            startActivity<SymptomsActivity>("symptoms" to symptoms)
                        else
                            toast("No details provided!")
                    }
                    else -> {
                    }
                }
            })
        } else {
            toast("No test details available.")
        }

    }

    private fun generatePairList(checkups: ArrayList<Checkup>): ArrayList<Pair<String, String?>> {

        val testTitles = resources.getStringArray(R.array.feedback_user_test)
        val testPairs = ArrayList<Pair<String, String?>>()

        // Feedback from Doctor
        val totalCheckups = checkups.size
        val lastCheckup = checkups[totalCheckups - 1].date
        testPairs.add(Pair<String, String?>(testTitles[0], lastCheckup))

        // Prescription
        val totalPrescriptions = checkups[0].prescription?.size ?: 0
        val lastPrescription = if (totalPrescriptions != 0) checkups[0].prescription?.get(totalPrescriptions - 1)?.date ?: "Never" else "Never"
        testPairs.add(Pair<String, String?>(testTitles[1], lastPrescription))

        // Reports
        testPairs.add(Pair<String, String?>(testTitles[2], "" /*Empty*/))

        // Symptoms
        testPairs.add(Pair<String, String?>(testTitles[3], "" /*Empty*/))

        // Vitals
        testPairs.add(Pair<String, String?>(testTitles[4], lastCheckup))

        // Generic info
        testPairs.add(Pair<String, String?>(testTitles[5], ""))

        return testPairs

    }

}