package com.jiovio.olivewear.rural.savemom.data

import android.bluetooth.BluetoothDevice
import android.os.Parcel

data class OliveWear(var bluetoothDevice: BluetoothDevice) : BaseParcelable {

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.write(bluetoothDevice)
    }

    companion object {
        @JvmField
        val CREATOR = BaseParcelable.generateCreator {
            OliveWear(it.read())
        }
    }
}
