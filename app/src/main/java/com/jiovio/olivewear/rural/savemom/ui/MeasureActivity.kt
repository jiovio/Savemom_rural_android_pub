package com.jiovio.olivewear.rural.savemom.ui

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.api.model.Patient
import com.jiovio.olivewear.rural.savemom.fragment.BaseFragment
import com.jiovio.olivewear.rural.savemom.fragment.MeasureConnFragment
import com.jiovio.olivewear.rural.savemom.fragment.MeasureFragment
import com.jiovio.olivewear.rural.savemom.util.commitTransaction
import com.jiovio.olivewear.rural.savemom.util.parseColor
import kotlinx.android.synthetic.main.activity_measure.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton
import org.jetbrains.anko.textColor

/**
 * Created by abara on 17/08/17.
 */
class MeasureActivity : BaseActivity(), MeasureFragment.OnMeasureSuccessListener {

    enum class FragmentType {
        HOME,
        IDENTITY,
        MEASURE_TYPE,
        BP,
        SPO,
        BG,
        TEMPERATURE,
        WEIGHT,
        SUMMARY,
        REPORT
    }

    private var patient: Patient? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_measure)
        measure_appbar.title = ""
        setSupportActionBar(measure_appbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        patient = intent.getParcelableExtra("patient")
        if (patient == null) {
            alert("Invalid Patient details!") {
                okButton { finish() }
            }.show()
        } else {
            loadFragment(MeasureConnFragment.getInstance(FragmentType.BP.name), false)
        }

    }

    /*
    * TODO: Implement measured values into patient.
    * */
    override fun onMeasureSuccess(fragmentType: Int, value: String) {
        // TODO: Insert measured values into Patient.
        // patient?.bp = value
    }

    override fun changeToolbarColor(color: Int, titleColor: Int) {
        supportActionBar?.setBackgroundDrawable(ColorDrawable(parseColor(color)))
        measure_appbar_title.textColor = parseColor(titleColor)
    }

    override fun loadFragment(fragment: BaseFragment, addToBackStack: Boolean) {
        supportFragmentManager.commitTransaction {
            if (addToBackStack) addToBackStack(null)
            replace(R.id.measure_content, fragment)
        }
    }

}