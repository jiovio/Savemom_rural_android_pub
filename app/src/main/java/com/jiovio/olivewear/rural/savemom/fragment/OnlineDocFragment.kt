package com.jiovio.olivewear.rural.savemom.fragment

import android.os.Bundle
import com.jiovio.olivewear.rural.savemom.adapter.OnlineDocAdapter
import com.jiovio.olivewear.rural.savemom.ui.TodoActivity
import kotlinx.android.synthetic.main.fragment_list.*
import org.jetbrains.anko.support.v4.intentFor

/**
 * Created by abara on 18/09/17.
 */
class OnlineDocFragment : ListFragment() {

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        fragment_list.adapter = OnlineDocAdapter({
            startActivity(intentFor<TodoActivity>())
        }, {
            nextFragment(OnlineDocDetailsFragment())
        })

    }

}