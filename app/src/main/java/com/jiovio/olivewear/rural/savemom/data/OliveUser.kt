package com.jiovio.olivewear.rural.savemom.data

import android.os.Parcel

data class OliveUser(var aadhaarId: String = "",
                     var name: String = "",
                     var mobile: String = "",
                     var photo: String = "",
                     var bloodPressure: Float = 0f,
                     var spo2: Float = 0f,
                     var bloodGlucose: Float = 0f,
                     var weight: Float = 0f,
                     var height: Float = 0f,
                     var temperature: Float = 0f) : BaseParcelable {
    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.write(aadhaarId)
        dest.write(name)
        dest.write(mobile)
        dest.write(photo)
        dest.write(bloodPressure)
        dest.write(spo2)
        dest.write(bloodGlucose)
        dest.write(weight)
        dest.write(height)
        dest.write(temperature)
    }

    companion object {
        @JvmField val CREATOR = BaseParcelable.generateCreator {
            OliveUser(it.read(), it.read(), it.read(), it.read(), it.read(), it.read(), it.read(), it.read(), it.read(), it.read())
        }
    }
}
