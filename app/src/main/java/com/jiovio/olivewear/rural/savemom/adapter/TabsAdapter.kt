package com.jiovio.olivewear.rural.savemom.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.jiovio.olivewear.rural.savemom.fragment.BaseFragment

/**
 * Created by abara on 26/08/17.
 */
class TabsAdapter(private val tabs: Array<String>, private val fragments: Array<BaseFragment>, fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getPageTitle(position: Int): CharSequence {
        return tabs[position]
    }

    override fun getItem(position: Int): Fragment = fragments[position]


    override fun getCount(): Int {
        return tabs.size
    }

}