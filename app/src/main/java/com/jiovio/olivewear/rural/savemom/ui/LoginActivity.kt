package com.jiovio.olivewear.rural.savemom.ui

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.api.ApiClient
import com.jiovio.olivewear.rural.savemom.api.ApiService
import com.jiovio.olivewear.rural.savemom.api.model.Worker
import com.jiovio.olivewear.rural.savemom.api.model.WorkerBody
import com.jiovio.olivewear.rural.savemom.util.AppPrefs
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.login_layout.*
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * Created by abara on 16/08/17.
 */
class LoginActivity : AppCompatActivity(), Callback<Worker> {

    private lateinit var dialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        login_button.setOnClickListener {

            val mobileNo = login_mobile_input_box.text.toString()
            if (!mobileNo.isEmpty()) {
                dialog = indeterminateProgressDialog("Logging in...", "", {
                    setCancelable(false)
                })
                val service = ApiClient.client.create(ApiService::class.java)
                service.loginWorker(WorkerBody(mobileNo)).enqueue(this)
            }

        }

    }

    override fun onResponse(call: Call<Worker>?, response: Response<Worker>) {
        val worker = response.body()
        if (worker != null) {
            if (worker.success) {
                loginSuccess(worker)
            } else {
                toast("Mobile number not registered.")
            }
        } else {
            toast("Login failed!")
        }
        dialog.dismiss()
    }

    override fun onFailure(call: Call<Worker>?, t: Throwable?) {
        t?.printStackTrace()
        toast("Try again.")
        dialog.dismiss()
    }

    private fun loginSuccess(worker: Worker) {
        loginLayout.visibility = View.GONE
        loginSplashLayout.visibility = View.VISIBLE
        hideKeyboard()

        val prefs = AppPrefs(this)
        prefs.loggedIn = true
        prefs.worker = worker
        prefs.accessToken = worker.token!!

        Timer().schedule(object : TimerTask() {
            override fun run() {
                startActivity(intentFor<MainActivity>())
                finish()
            }
        }, 2000L)
    }

    private fun hideKeyboard() {
        try {
            val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(login_layout.windowToken, 0)
        } catch (e: Exception) {
            // Nothing
        }
    }

}