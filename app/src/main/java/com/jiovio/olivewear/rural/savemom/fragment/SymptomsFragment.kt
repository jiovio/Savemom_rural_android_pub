package com.jiovio.olivewear.rural.savemom.fragment

import android.os.Bundle
import android.support.v7.widget.AppCompatRadioButton
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.api.model.Symptoms
import kotlinx.android.synthetic.main.fragment_symptoms_five.*
import kotlinx.android.synthetic.main.fragment_symptoms_four.*
import kotlinx.android.synthetic.main.fragment_symptoms_one.*
import kotlinx.android.synthetic.main.fragment_symptoms_three.*
import kotlinx.android.synthetic.main.fragment_symptoms_two.*

/**
 * Created by abara on 18/09/17.
 */
class SymptomsFragment : BaseFragment() {

    companion object {
        private val YES = "yes"
        val ONE = 1
        val TWO = 2
        val THREE = 3
        val FOUR = 4
        val FIVE = 5
        fun load(symptoms: Symptoms, type: Int): SymptomsFragment {
            val fragment = SymptomsFragment()
            val args = Bundle()
            args.putInt("type", type)
            args.putParcelable("symptoms", symptoms)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        when (arguments.getInt("type")) {
            ONE -> return inflater.inflate(R.layout.fragment_symptoms_one, container, false)
            TWO -> return inflater.inflate(R.layout.fragment_symptoms_two, container, false)
            THREE -> return inflater.inflate(R.layout.fragment_symptoms_three, container, false)
            FOUR -> return inflater.inflate(R.layout.fragment_symptoms_four, container, false)
            FIVE -> return inflater.inflate(R.layout.fragment_symptoms_five, container, false)
        }
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val symptoms = arguments.getParcelable<Symptoms>("symptoms")

        when (arguments.getInt("type")) {
            ONE -> {
                toolbarColor(R.color.colorAccentLite)
                checkYesOrNo(symptoms.headache, fragment_symptoms_headache_yes, fragment_symptoms_headache_no)
                checkYesOrNo(symptoms.blurredvision, fragment_symptoms_blurredvision_yes, fragment_symptoms_blurredvision_no)
                checkYesOrNo(symptoms.dizziness, fragment_symptoms_dizzy_yes, fragment_symptoms_dizzy_no)
                checkYesOrNo(symptoms.shortnessofbreath, fragment_symptoms_breath_yes, fragment_symptoms_breath_no)
                checkYesOrNo(symptoms.legedema, fragment_symptoms_cough_phlegm_yes, fragment_symptoms_cough_phlegm_no)
                checkYesOrNo(symptoms.chestpain, fragment_symptoms_chest_pain_yes, fragment_symptoms_chest_pain_no)
                checkYesOrNo(symptoms.abdominalpain, fragment_symptoms_abdominalpain_yes, fragment_symptoms_abdominalpain_no)
            }
            TWO -> {
                checkYesOrNo(symptoms.vomiting, fragment_symptoms_nausea_yes, fragment_symptoms_nausea_no)
                checkYesOrNo(symptoms.sourtaste, fragment_symptoms_sourtaste_yes, fragment_symptoms_sourtaste_no)
                checkYesOrNo(symptoms.stomachburning, fragment_symptoms_burning_yes, fragment_symptoms_burning_no)
                checkYesOrNo(symptoms.spotting, fragment_symptoms_spotting_yes, fragment_symptoms_spotting_no)
                checkYesOrNo(symptoms.bleeding, fragment_symptoms_bleeding_yes, fragment_symptoms_bleeding_no)
                fragment_symptoms_bleeding_pads.setText(symptoms.noofpads)
            }
            THREE -> {
                checkYesOrNo(symptoms.amnioticfluid, fragment_symptoms_amniotic_yes, fragment_symptoms_amniotic_no)
                checkYesOrNo(symptoms.urinaryburning, fragment_symptoms_urinary_burning_yes, fragment_symptoms_urinary_burning_no)
                checkYesOrNo(symptoms.vaginalitching, fragment_symptoms_vaginal_itching_yes, fragment_symptoms_vaginal_itching_no)
                checkYesOrNo(symptoms.vaginaldischarge, fragment_symptoms_vaginal_discharge_yes, fragment_symptoms_vaginal_discharge_no)
                fragment_symptoms_color_discharge.setText(symptoms.colorofdischarge)
                fragment_symptoms_urinary_frequency.setText(symptoms.urinaryfrequency)
            }
            FOUR -> {
                checkYesOrNo(symptoms.feverandchills, fragment_symptoms_fever_chills_yes, fragment_symptoms_fever_chills_no)
                checkYesOrNo(symptoms.rashesonskin, fragment_symptoms_rashes_skin_yes, fragment_symptoms_rashes_skin_no)
                checkYesOrNo(symptoms.coughandphlegm, fragment_symptoms_cough_phlegm_yes, fragment_symptoms_cough_phlegm_no)
                fragment_symptoms_color_phlegm.setText(symptoms.colorofphlegm)
                checkYesOrNo(symptoms.asthma, fragment_symptoms_asthma_yes, fragment_symptoms_asthma_no)
                checkYesOrNo(symptoms.bodyaches, fragment_symptoms_body_aches_yes, fragment_symptoms_body_aches_no)
            }
            FIVE -> {
                fragment_symptoms_discomforts.setText(symptoms.anydiscomforts)
            }
        }

    }

    private fun checkYesOrNo(yesOrNoString: String?, yesBox: AppCompatRadioButton, noBox: AppCompatRadioButton) {

        if (yesOrNoString != null) {
            if (YES.contentEquals(yesOrNoString.toLowerCase())) {
                yesBox.isChecked = true
            } else {
                noBox.isChecked = true
            }
        } else {
            noBox.isChecked = true
        }

    }

}