package com.jiovio.olivewear.rural.savemom.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.ui.MainActivity
import com.jiovio.olivewear.rural.savemom.ui.MeasureActivity
import kotlinx.android.synthetic.main.activity_main.*

/**
 * Created by abara on 16/08/17.
 */
open class BaseFragment : Fragment() {

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        changeTheme()
        super.onActivityCreated(savedInstanceState)
    }

    fun changeTheme(themeId: Int = R.style.AppTheme) {
        // Default to black status bar
        when (activity) {
            is MainActivity -> (activity as MainActivity).changeTheme(themeId)
            is MeasureActivity -> (activity as MeasureActivity).changeTheme(themeId)
        }
    }

    fun nextFragment(fragment: BaseFragment, backStack: Boolean = true) {
        when (activity) {
            is MainActivity -> (activity as MainActivity).loadFragment(fragment, backStack)
            is MeasureActivity -> (activity as MeasureActivity).loadFragment(fragment, backStack)
        }
    }

    fun setTitle(title: String) {
        when (activity) {
            is MainActivity -> (activity as MainActivity).main_appbar_title.text = title
            is MeasureActivity -> (activity as MeasureActivity).main_appbar_title.text = title
        }
    }

    fun setTitle(title: Int) {
        when (activity) {
            is MainActivity -> (activity as MainActivity).main_appbar_title.setText(title)
            is MeasureActivity -> (activity as MeasureActivity).main_appbar_title.setText(title)
        }
    }

    fun toolbarColor(color: Int = R.color.colorPrimary, titleColor: Int = R.color.colorBlack) {
        when (activity) {
            is MainActivity -> (activity as MainActivity).changeToolbarColor(color, titleColor)
            is MeasureActivity -> (activity as MeasureActivity).changeToolbarColor(color, titleColor)
        }
    }

}