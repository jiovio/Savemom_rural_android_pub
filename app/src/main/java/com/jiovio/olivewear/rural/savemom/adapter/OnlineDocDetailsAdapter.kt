package com.jiovio.olivewear.rural.savemom.adapter

import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.view.QuickSandTextView

/**
 * Created by abara on 26/08/17.
 */
class OnlineDocDetailsAdapter : RecyclerView.Adapter<OnlineDocDetailsAdapter.OnlineDocDetailsHolder>() {

    override fun onBindViewHolder(holder: OnlineDocDetailsHolder, position: Int) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OnlineDocDetailsHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.single_item_online_doc_details, parent, false)
        return OnlineDocDetailsHolder(view)
    }

    override fun getItemCount(): Int = 1

    class OnlineDocDetailsHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title = view.findViewById<QuickSandTextView>(R.id.item_online_doc_details_title)
        val desc = view.findViewById<QuickSandTextView>(R.id.item_online_doc_details_desc)
        val addTask = view.findViewById<AppCompatButton>(R.id.item_online_doc_details_addtask)
    }

}