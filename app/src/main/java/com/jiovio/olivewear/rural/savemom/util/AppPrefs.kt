package com.jiovio.olivewear.rural.savemom.util

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.google.gson.Gson
import com.jiovio.olivewear.rural.savemom.api.model.Worker

/**
 * Created by abara on 16/08/17.
 */
class AppPrefs(private val context: Context) {

    private val prefs: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(context.applicationContext)
    }

    var loggedIn: Boolean
        set(value) = prefs.edit().putBoolean("logged_in", value).apply()
        get() = prefs.getBoolean("logged_in", false)

    var worker: Worker
        set(value) = prefs.edit().putString("worker_object", Gson().toJson(value)).apply()
        get() = Gson().fromJson(prefs.getString("worker_object", "{}"), Worker::class.java)

    var accessToken: String
        set(value) = prefs.edit().putString("access_token", value).apply()
        get() = prefs.getString("access_token", "")

}