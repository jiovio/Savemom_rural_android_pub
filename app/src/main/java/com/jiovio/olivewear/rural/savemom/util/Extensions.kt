package com.jiovio.olivewear.rural.savemom.util

import android.content.Context
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v4.content.ContextCompat

/**
 * Created by abara on 16/08/17.
 */

// Context
fun Context.parseColor(color: Int): Int = ContextCompat.getColor(this, color)

// Fragment Transaction
fun FragmentManager.commitTransaction(code: FragmentTransaction.() -> FragmentTransaction) = beginTransaction().code().commit()