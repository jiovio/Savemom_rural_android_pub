package com.jiovio.olivewear.rural.savemom.adapter

import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.view.QuickSandTextView

/**
 * Created by abara on 27/08/17.
 */
class TabletsAdapter : RecyclerView.Adapter<TabletsAdapter.TabletsHolder>() {

    override fun onBindViewHolder(holder: TabletsHolder?, position: Int) {

    }

    override fun getItemCount(): Int = 2

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TabletsHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.single_item_tablets, parent, false)
        return TabletsHolder(view)
    }

    class TabletsHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title = view.findViewById<QuickSandTextView>(R.id.single_item_tablets_title)
        val desc = view.findViewById<QuickSandTextView>(R.id.single_item_tablets_desc)
        val doneBtn = view.findViewById<AppCompatButton>(R.id.single_item_tablets_done)
    }

}