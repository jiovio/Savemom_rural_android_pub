package com.jiovio.olivewear.rural.savemom.api

import com.jiovio.olivewear.rural.savemom.api.model.NewPatient
import com.jiovio.olivewear.rural.savemom.api.model.Patient
import com.jiovio.olivewear.rural.savemom.api.model.Worker
import com.jiovio.olivewear.rural.savemom.api.model.WorkerBody
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by abara on 10/09/17.
 */

interface ApiService {

    @Headers("Content-Type: application/json")
    @POST("healthworkerlogin")
    fun loginWorker(@Body body: WorkerBody): Call<Worker>

    @GET("patient/{aadharno}")
    fun getPatient(@Path("aadharno") aadharNo: String, @Header("x-access-token") x_access_token: String): Call<Patient>

    @POST("patient")
    fun createPatient(@Body patient: Patient, @Header("x-access-token") x_access_token: String): Call<NewPatient>

    @GET("patient/")
    fun listPatients(@Header("x-access-token") x_access_token: String): Call<List<Patient>>

}
