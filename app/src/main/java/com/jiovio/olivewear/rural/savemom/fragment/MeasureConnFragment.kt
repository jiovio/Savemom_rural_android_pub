package com.jiovio.olivewear.rural.savemom.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.mcloud.MeasureHelper
import com.jiovio.olivewear.rural.savemom.ui.MeasureActivity.FragmentType.*
import com.jiovio.olivewear.rural.savemom.util.BluetoothUtils
import kotlinx.android.synthetic.main.fragment_bp_connection.*
import org.jetbrains.anko.support.v4.toast

/**
 * Created by abara on 27/08/17.
 */
class MeasureConnFragment : BaseFragment() {

    private var currentMeasureType = BP

    companion object {
        private val EXTRA_TYPE = "extra_type"
        fun getInstance(fragmentType: String): MeasureConnFragment {
            val fragment = MeasureConnFragment()
            val args = Bundle()
            args.putString(EXTRA_TYPE, fragmentType)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        currentMeasureType = valueOf(arguments.getString(EXTRA_TYPE, BP.name))
        return inflater.inflate(R.layout.fragment_bp_connection, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        checkBluetooth()

    }

    private fun checkBluetooth(): Boolean {
        if (!BluetoothUtils.checkPermission(this)) {
            if (BluetoothUtils.enabled(this)) {
                BluetoothUtils.startDiscovery()
                return true
            }
        } else {
            if (BluetoothUtils.enabled(this)) {
                BluetoothUtils.startDiscovery()
                return true
            }
        }
        return false
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toolbarColor(getBgColor(), R.color.colorWhite)

        fragment_bp_device_next.setOnClickListener {
            if (checkBluetooth()) {
                nextFragment(MeasureFragment.getInstance(currentMeasureType.name), false)
            } else {
                toast("Enable Bluetooth")
            }
        }

    }


    fun getBgColor(): Int = when (currentMeasureType) {
        BP -> R.color.colorTeal
        else -> R.color.colorTeal
    }

    fun getMeasureType(): Int = when (currentMeasureType) {
        SPO -> MeasureHelper.BLOOD_OXYGEN_ALL
        BG -> MeasureHelper.BLOOD_SUGAR
        TEMPERATURE -> MeasureHelper.BASE_TEMPERATURE
        else -> MeasureHelper.BLOOD_PRESSURE_ALL
    }

    fun getTitle(): Int = when (currentMeasureType) {
        BG -> R.string.blood_glucose_title
        BP -> R.string.blood_pressure_title
        else -> R.string.blood_pressure_title
    }


}