package com.jiovio.olivewear.rural.savemom.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.jiovio.olivehealth.rural.savemom.R

/**
 * Created by abara on 26/08/17.
 */
class FeedbackTestAdapter(private val testPairs: ArrayList<Pair<String, String?>>, private val clickAction: (pos: Int) -> Unit) : RecyclerView.Adapter<FeedbackItemAdapter.FeedbackItemHolder>() {

    private lateinit var context: Context

    override fun onBindViewHolder(holder: FeedbackItemAdapter.FeedbackItemHolder, position: Int) {
        val pair = testPairs[position]
        holder.title.text = pair.first
        holder.desc.text = context.resources.getString(R.string.feedback_last_update, pair.second ?: "Never")
        holder.layout.setOnClickListener {
            clickAction(holder.adapterPosition)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedbackItemAdapter.FeedbackItemHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.single_item_feedback, parent, false)
        return FeedbackItemAdapter.FeedbackItemHolder(view)
    }

    override fun getItemCount(): Int = testPairs.size

}