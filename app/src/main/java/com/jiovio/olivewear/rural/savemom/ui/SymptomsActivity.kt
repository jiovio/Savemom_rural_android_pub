package com.jiovio.olivewear.rural.savemom.ui

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.api.model.Symptoms
import com.jiovio.olivewear.rural.savemom.fragment.BaseFragment
import com.jiovio.olivewear.rural.savemom.fragment.SymptomsFragment
import com.jiovio.olivewear.rural.savemom.util.commitTransaction
import com.jiovio.olivewear.rural.savemom.util.parseColor
import kotlinx.android.synthetic.main.activity_symptoms.*
import org.jetbrains.anko.textColor

/**
 * Created by abara on 17/08/17.
 */
class SymptomsActivity : BaseActivity() {

    private val fragmentTypes = arrayOf(SymptomsFragment.ONE, SymptomsFragment.TWO, SymptomsFragment.THREE,
            SymptomsFragment.FOUR, SymptomsFragment.FIVE)
    private var typeChanger = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_symptoms)
        symptoms_appbar.title = ""
        setSupportActionBar(symptoms_appbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val symptoms = intent.getParcelableExtra<Symptoms>("symptoms")

        fragment_symptoms_next_fab.setOnClickListener {

            if (typeChanger != fragmentTypes.size)
                loadFragment(SymptomsFragment.load(symptoms, typeChanger++))

        }

    }

    override fun changeToolbarColor(color: Int, titleColor: Int) {
        supportActionBar?.setBackgroundDrawable(ColorDrawable(parseColor(color)))
        symptoms_appbar_title.textColor = parseColor(titleColor)
    }

    override fun loadFragment(fragment: BaseFragment, addToBackStack: Boolean) {
        supportFragmentManager.commitTransaction {
            if (addToBackStack) addToBackStack(null)
            replace(R.id.symptoms_content, fragment)
        }
    }


}
