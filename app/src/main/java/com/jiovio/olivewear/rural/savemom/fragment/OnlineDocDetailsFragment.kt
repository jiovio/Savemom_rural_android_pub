package com.jiovio.olivewear.rural.savemom.fragment

import android.os.Bundle
import com.jiovio.olivewear.rural.savemom.adapter.OnlineDocDetailsAdapter
import kotlinx.android.synthetic.main.fragment_list.*

/**
 * Created by abara on 18/09/17.
 */
class OnlineDocDetailsFragment : ListFragment() {

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        fragment_list.adapter = OnlineDocDetailsAdapter()

    }

}