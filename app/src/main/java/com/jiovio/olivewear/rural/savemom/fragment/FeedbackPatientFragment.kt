package com.jiovio.olivewear.rural.savemom.fragment

import android.os.Bundle
import com.jiovio.olivewear.rural.savemom.adapter.FeedbackUserAdapter
import com.jiovio.olivewear.rural.savemom.api.model.Checkup
import kotlinx.android.synthetic.main.fragment_list.*
import org.jetbrains.anko.support.v4.toast

/**
 * Created by abara on 16/09/17.
 */
class FeedbackPatientFragment : ListFragment() {

    companion object {
        fun getInstance(title: String, checkups: List<Checkup>?): FeedbackPatientFragment {
            val fragment = FeedbackPatientFragment()
            val args = Bundle()
            checkups?.let {
                args.putParcelableArrayList("checkups", ArrayList(checkups))
            }
            args.putString("title", title)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val toolbarTitle = arguments.getString("title")
        setTitle(toolbarTitle)

        val checkups = arguments.getParcelableArrayList<Checkup>("checkups")
        if (checkups != null) {
            fragment_list.adapter = FeedbackUserAdapter(checkups, { title, pos ->
                nextFragment(FeedbackTestsFragment.getInstance(title, checkups))
            })
        } else {
            toast("No checkups made.")
        }

    }

}