package com.jiovio.olivewear.rural.savemom.fragment

import android.app.ProgressDialog
import android.os.Bundle
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.adapter.FeedbackItemAdapter
import com.jiovio.olivewear.rural.savemom.api.ApiClient
import com.jiovio.olivewear.rural.savemom.api.ApiService
import com.jiovio.olivewear.rural.savemom.api.model.Patient
import com.jiovio.olivewear.rural.savemom.util.AppPrefs
import kotlinx.android.synthetic.main.fragment_list.*
import org.jetbrains.anko.support.v4.indeterminateProgressDialog
import org.jetbrains.anko.support.v4.toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by abara on 16/09/17.
 */
class FeedbackFragment : ListFragment(), Callback<List<Patient>> {

    private lateinit var listPatientsCall: Call<List<Patient>>
    private var adapter: FeedbackItemAdapter? = null
    private var dialog: ProgressDialog? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setTitle(R.string.today_group_feedback_title)

        if (adapter != null) {
            fragment_list.adapter = adapter
        } else {
            dialog = indeterminateProgressDialog("Loading patients...", "", {
                setCancelable(false)
            })
            val service = ApiClient.client.create(ApiService::class.java)
            listPatientsCall = service.listPatients(AppPrefs(activity).accessToken)
            listPatientsCall.enqueue(this)
        }

    }

    override fun onFailure(call: Call<List<Patient>>?, t: Throwable?) {
        toast("No patients")
        dialog?.dismiss()
    }

    override fun onResponse(call: Call<List<Patient>>?, response: Response<List<Patient>>?) {

        if (response != null) {

            val patients = response.body()

            if (patients != null) {
                adapter = FeedbackItemAdapter(patients, { title, pos ->
                    val checkups = patients[pos].checkup
                    nextFragment(FeedbackPatientFragment.getInstance(title, checkups))
                })
                fragment_list.adapter = adapter
            } else {
                toast("No patients available.")
            }

        } else {
            toast("Server error, try again.")
        }
        dialog?.dismiss()

    }

    override fun onDetach() {
        super.onDetach()
        if (!listPatientsCall.isExecuted)
            listPatientsCall.cancel()
    }

}