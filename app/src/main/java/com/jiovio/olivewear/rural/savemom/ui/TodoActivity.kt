package com.jiovio.olivewear.rural.savemom.ui

import android.os.Bundle
import android.view.MenuItem
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.adapter.CheckupAdapter
import com.jiovio.olivewear.rural.savemom.adapter.TabletsAdapter
import com.jiovio.olivewear.rural.savemom.adapter.TabsAdapter
import com.jiovio.olivewear.rural.savemom.fragment.BaseFragment
import com.jiovio.olivewear.rural.savemom.fragment.ListFragment
import kotlinx.android.synthetic.main.activity_todo.*

/**
 * Created by abara on 25/08/17.
 */
class TodoActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_todo)

        todo_appbar.title = ""
        setSupportActionBar(todo_appbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val titles = resources.getStringArray(R.array.todo_tabs)

        /*todo_viewpager.adapter = TabsAdapter(titles, arrayOf(
                ListFragment.getInstance(title = "", adapter = TabletsAdapter()),
                ListFragment.getInstance(title = "", adapter = CheckupAdapter()),
                ListFragment.getInstance(title = "", adapter = CheckupAdapter())
        ), supportFragmentManager)*/
        todo_tablayout.setupWithViewPager(todo_viewpager)
    }

    override fun changeToolbarColor(color: Int, titleColor: Int) {
        // Nothing
    }

    override fun loadFragment(fragment: BaseFragment, addToBackStack: Boolean) {
        // Nothing
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

}