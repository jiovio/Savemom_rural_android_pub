package com.jiovio.olivewear.rural.savemom.ui

import android.app.ProgressDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatRadioButton
import android.view.Menu
import android.view.MenuItem
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.api.ApiClient
import com.jiovio.olivewear.rural.savemom.api.ApiService
import com.jiovio.olivewear.rural.savemom.api.model.NewPatient
import com.jiovio.olivewear.rural.savemom.api.model.Patient
import com.jiovio.olivewear.rural.savemom.fragment.NewUserFragments
import kotlinx.android.synthetic.main.activity_newuser.*
import kotlinx.android.synthetic.main.fragment_newuser_medicalhx.*
import kotlinx.android.synthetic.main.fragment_newuser_personalinfo.*
import kotlinx.android.synthetic.main.fragment_newuser_socialhx.*
import org.jetbrains.anko.appcompat.v7.navigationIconResource
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by abara on 16/08/17.
 */
class NewUserActivity : AppCompatActivity(), Callback<NewPatient> {

    private lateinit var dialog: ProgressDialog

    override fun onResponse(call: Call<NewPatient>?, response: Response<NewPatient>?) {
        if (response != null) {
            val resp = response.body()
            if (resp?._id != null) {
                toast("Patient created!")
                finish()
            } else {
                toast("Cannot create new patient.\nTry again.")
            }
        } else {
            toast("Cannot create new patient.\nTry again.")
        }
        dialog.dismiss()
    }

    override fun onFailure(call: Call<NewPatient>?, t: Throwable?) {
        toast("Cannot create new patient.\nTry again.")
        dialog.dismiss()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_newuser)
        newuser_appbar.title = ""
        newuser_appbar.navigationIconResource = R.drawable.ic_close
        setSupportActionBar(newuser_appbar)

        newuser_viewpager.offscreenPageLimit = 3
        newuser_viewpager.adapter = NewUserTabsAdapter(supportFragmentManager)
        newuser_tablayout.setupWithViewPager(newuser_viewpager)

    }

    inner class NewUserTabsAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        private val tabs = resources.getStringArray(R.array.newuser_tabs)

        override fun getPageTitle(position: Int): CharSequence {
            return tabs[position]
        }

        override fun getItem(position: Int): Fragment =
                when (position) {
                    0 -> NewUserFragments.get(NewUserFragments.PERSONAL_INFO)
                    1 -> NewUserFragments.get(NewUserFragments.MEDICAL_INFO)
                    2 -> NewUserFragments.get(NewUserFragments.SOCIAL_INFO)
                    else -> NewUserFragments.get(NewUserFragments.PERSONAL_INFO)
                }


        override fun getCount(): Int {
            return tabs.size
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.new_user, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
            R.id.newuser_action_done -> createUser()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun createUser() {

        dialog = indeterminateProgressDialog("Creating user...", "", {
            setCancelable(false)
        })
        val patient = createPatient()
        val service = ApiClient.client.create(ApiService::class.java)
        service.createPatient(patient, "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im9saXZld2VhcmFkbWluIiwiaWF0IjoxNTA1MDU4MTEzfQ.GIZx5qbMwcVnwgVJ7gX334WmnV00vpBFG7WjIGwKhq4")
                .enqueue(this)

    }

    private fun createPatient(): Patient {
        val patient = Patient()

        patient.name = newuser_personalinfo_name.text.toString()
        patient.aadharno = newuser_personalinfo_adhaar.text.toString()
        patient.age = newuser_personalinfo_age_name.text.toString()
        patient.fathername = newuser_personalinfo_father_name.text.toString()
        patient.husbandname = newuser_personalinfo_husband_name.text.toString()
        patient.villagename = newuser_personalinfo_colony_name.text.toString()
        patient.district = newuser_personalinfo_district_name.text.toString()
        patient.state = newuser_personalinfo_state_name.text.toString()
        patient.pincode = newuser_personalinfo_pincode_name.text.toString()
        patient.age = newuser_personalinfo_age_name.text.toString()
        patient.duedate = newuser_personalinfo_duedate_name.text.toString()
        patient.phoneno = newuser_personalinfo_phone_number.text.toString()
        patient.childdelivered = newuser_personalinfo_children_del.text.toString()
        patient.complications = newuser_personalinfo_complications.text.toString()
        patient.nooflastcarevisit = newuser_personalinfo_visits.text.toString()
        patient.childborn = getRadioText(newuser_personalinfo_born_grp.checkedRadioButtonId)

        patient.ageofchild = newuser_medicalhx_childrenage.text.toString()
        patient.firstchildbornage = newuser_medicalhx_firstage.text.toString()
        patient.diabeticsparent = getRadioText(newuser_medicalhx_diabetes_grp.checkedRadioButtonId)
        patient.bpparent = getRadioText(newuser_medicalhx_bp_grp.checkedRadioButtonId)
        patient.cancerparent = getRadioText(newuser_medicalhx_cancer_grp.checkedRadioButtonId)
        patient.othermedicalissueparent = newuser_medicalhx_medical_issues.text.toString()

        patient.coffee = getRadioText(newuser_socialhx_coffee_grp.checkedRadioButtonId)
        patient.tea = getRadioText(newuser_socialhx_tea_grp.checkedRadioButtonId)
        patient.tobaccoo = getRadioText(newuser_socialhx_tobacco_grp.checkedRadioButtonId)
        patient.alhocol = getRadioText(newuser_socialhx_alcohol_grp.checkedRadioButtonId)
        patient.alhocolhusband = getRadioText(newuser_socialhx_alcohol_husband_grp.checkedRadioButtonId)
        patient.tobaccoohusband = getRadioText(newuser_socialhx_tobacco_husband_grp.checkedRadioButtonId)
        patient.hiv = getRadioText(newuser_socialhx_hiv_grp.checkedRadioButtonId)
        patient.sexualabuse = getRadioText(newuser_socialhx_sexual_abuse_grp.checkedRadioButtonId)

        return patient
    }

    private fun getRadioText(checkedRadioButtonId: Int): String {
        return findViewById<AppCompatRadioButton>(checkedRadioButtonId).text.toString()
    }
}