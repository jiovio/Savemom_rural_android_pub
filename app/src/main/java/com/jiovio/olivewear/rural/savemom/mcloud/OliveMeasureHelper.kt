package com.jiovio.olivewear.rural.savemom.mcloud

import android.os.Message
import com.medzone.mcloud.background.BluetoothMessage
import com.medzone.mcloud.background.util.BluetoothUtils
import java.util.regex.Pattern


class OliveMeasureHelper : MeasureHelper.ICommandCallback {

    interface OliveWearListener {
        fun onDeviceConnecting()
        fun onDeviceSearch()
        fun onDeviceNotFound()
        fun onDeviceConnectionFailure()
        fun onDeviceConnected()
        fun onDeviceDisconnected()
        fun onDeviceException()
        fun onDevicePowerLow()
        fun onProgress()
        fun onResult(result: String)
    }

    private lateinit var mOliveWearListener: OliveWearListener

    fun init(oliveWearListener: OliveWearListener, measureType: Int, deviceAddress: String) {
        this.mOliveWearListener = oliveWearListener
        MeasureHelper.addListener(this)
        MeasureHelper.open(measureType, deviceAddress)
    }

    override fun handleMessage(msg: Message) {
        when (msg.what) {
        // MeasureHelper.SHOW_DEVICE_LIST, MeasureHelper.DEVICE_DETECTED, MeasureHelper.HIDE_DEVICE_LIST

            MeasureHelper.UPDATE_STATUS ->
                when (msg.arg1) {
                    BluetoothMessage.msg_device_search_started ->
                        mOliveWearListener.onDeviceSearch()
                    BluetoothMessage.msg_device_search_error ->
                        mOliveWearListener.onDeviceNotFound()
                    BluetoothMessage.msg_socket_connecting ->
                        mOliveWearListener.onDeviceConnecting()
                    BluetoothMessage.msg_socket_connect_failed ->
                        mOliveWearListener.onDeviceConnectionFailure()
                    BluetoothMessage.msg_socket_connected ->
                        mOliveWearListener.onDeviceConnected()
                    BluetoothMessage.msg_socket_disconnected, BluetoothMessage.msg_device_disconnected ->
                        mOliveWearListener.onDeviceDisconnected()
                }

            MeasureHelper.MEASURE_RESULT -> {
                when (msg.arg1.toShort()) {
                    BluetoothUtils.MEASURE_PROGRESS -> mOliveWearListener.onProgress()
                    BluetoothUtils.START_MEASURE -> {
                        when (msg.arg2) {
                            0 ->
                                mOliveWearListener.onResult(getDisplay(msg.obj as String))
                            1 ->
                                mOliveWearListener.onDeviceException()
                            2 ->
                                mOliveWearListener.onDevicePowerLow()
                            3 ->
                                mOliveWearListener.onDeviceException()
                        }
                    }
                }
            }

        }
    }

    fun release() {
        MeasureHelper.removeListener(this)
        MeasureHelper.close()
    }

    private fun getDisplay(raw: String?): String {
        if (raw == null)
            return ""
        val splits = raw.split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        if (splits.size < 3)
            return ""

        if (!isNumeric(splits[2])) {
            return "SYS:" + splits[0] + "\n" +
                    "DIA" + splits[1] + "\n"
        }

        var plus = Integer.parseInt(splits[2])
        var irregularBeat = false
        if (plus > 0xFF00) {
            irregularBeat = true
            plus = plus and 0xFF
        }



        return "SYS:" + splits[0] + "\n" +
                "DIA" + splits[1] + "\n" +
                "Pulse:" + plus + (if (irregularBeat) "\nIrregular rhythm, measure again" else "") + "\n"

    }

    private fun isNumeric(str: String): Boolean {
        val pattern = Pattern.compile("[0-9]*")
        return pattern.matcher(str).matches()
    }

    fun measure() {
        MeasureHelper.measure()
    }
}
